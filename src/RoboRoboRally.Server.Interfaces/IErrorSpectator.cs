﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to non-game based errors.
    /// </summary>
    public interface IErrorSpectator : IClient
    {
        /// <summary>
        /// Represents an error that occured in the game logic.
        /// </summary>
        /// <param name="message"></param>
        void FatalRunningGameError(string message);

        /// <summary>
        /// Represents an unexpected error.
        /// </summary>
        /// <param name="message"></param>
        void FatalUnexpectedError(string message);
    }
}