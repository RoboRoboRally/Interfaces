﻿using System;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents the server. The first interface any client will communicate with.
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// The method every client will open his/hers connection with. Initializes the connection.
        /// </summary>
        /// <param name="clientName">The name of the client.</param>
        /// <param name="supportedClients">The types of the supported clients.</param>
        void Connect(string clientName, params Type[] supportedClients);

        /// <summary>
        /// Creates a new lobby and instantly joins it. The lobby is removed when it is empty.
        /// The lobby is joined as spectator.
        /// </summary>
        /// <param name="lobbyName">The name of the newly formed lobby. If null, the lobby name will be generated.</param>
        /// <returns>The newly created lobby.</returns>
        IOwnedLobby CreateLobby(string lobbyName = null);

        /// <summary>
        /// Joins a lobby with the specified <paramref name="lobbyId"/>. The lobby is joined as spectator
        /// </summary>
        /// <param name="lobbyId">Id of the lobby the client wishes to join.</param>
        /// <returns>The lobby the client joined.</returns>
        ILobby JoinLobby(string lobbyId);
    }

    /// <summary>
    /// The lobby is no longer available.
    /// </summary>
    [Serializable]
    public class LobbyNotFoundException : Exception
    {
        /// <inheritdoc/>
        public LobbyNotFoundException() { }

        /// <inheritdoc/>
        public LobbyNotFoundException(string message) : base(message) { }

        /// <inheritdoc/>
        public LobbyNotFoundException(string message, Exception inner) : base(message, inner) { }

        /// <inheritdoc/>
        protected LobbyNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}