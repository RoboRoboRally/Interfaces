﻿using System;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents the role of the client.
    /// </summary>
    public enum ClientRole
    {
        /// <summary>
        /// Represents that the client is only a spectator.
        /// </summary>
        Spectator,

        /// <summary>
        /// Represents that the client is a player.
        /// </summary>
        Player
    }

    /// <summary>
    /// Represents the lobby on server side.
    /// </summary>
    public interface ILobby : ILobbyInfo
    {
        /// <summary>
        /// Leaves the lobby.
        /// </summary>
        void LeaveLobby();

        /// <summary>
        /// Gets the role of the client. Either spectator or player.
        /// </summary>
        /// <returns>The role of the client.</returns>
        ClientRole GetMyRole();

        /// <summary>
        /// Attempts to change role to player, choosing the specified <paramref name="chosenRobot"/>.
        /// </summary>
        /// <param name="chosenRobot">The chosen robot</param>
        /// <exception cref="RobotAlreadySelectedException">When the robot is no longer available to play.</exception>
        void ChangeMyRoleToPlayer(RobotInfo chosenRobot);

        /// <summary>
        /// Frees the currently selected robot and becomes spectator only.
        /// </summary>
        void ChangeMyRoleToSpectator();

        /// <summary>
        /// Starts the game.
        /// </summary>
        /// <param name="settings">The settings the game will start with.</param>
        void StartGame(GameSettings settings);

        /// <summary>
        /// Gets a list of all plugins that can be added to the current lobby.
        /// </summary>
        PluginInfo[] GetAvailablePlugins();

        /// <summary>
        /// Gets a list of all connections currently registered in the lobby.
        /// </summary>
        ConnectionInfo[] GetAllConnections();
    }

    /// <summary>
    /// The robot has already been selected.
    /// </summary>
    [Serializable]
    public class RobotAlreadySelectedException : Exception
    {
        /// <inheritdoc/>
        public RobotAlreadySelectedException() { }

        /// <inheritdoc/>
        public RobotAlreadySelectedException(string message) : base(message) { }

        /// <inheritdoc/>
        public RobotAlreadySelectedException(string message, Exception inner) : base(message, inner) { }

        /// <inheritdoc/>
        protected RobotAlreadySelectedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// The plugin is no longer available.
    /// </summary>
    [Serializable]
    public class PluginNotAvailableException : Exception
    {
        /// <inheritdoc/>
        public PluginNotAvailableException() { }

        /// <inheritdoc/>
        public PluginNotAvailableException(string message) : base(message) { }

        /// <inheritdoc/>
        public PluginNotAvailableException(string message, Exception inner) : base(message, inner) { }

        /// <inheritdoc/>
        protected PluginNotAvailableException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}