﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a purchase
    /// </summary>
    public class PurchaseResponse
    {
        /// <summary>
        /// The selected card. Null if no card is being bought.
        /// </summary>
        public UpgradeCardInfo SelectedCard { get; set; }

        /// <summary>
        /// The discarded card if the player has too many cards of said type.
        /// </summary>
        public UpgradeCardInfo DiscardedCard { get; set; }

        /// <summary>
        /// Represents a null object.
        /// </summary>
        public static PurchaseResponse NothingBought => new PurchaseResponse
        {
            DiscardedCard = null,
            SelectedCard = null
        };
    }
}