﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents one instance of a game.
    /// </summary>
    public interface IGame
    {
        /// <summary>
        /// Pauses the game.
        /// </summary>
        void Pause();

        /// <summary>
        /// Resumes the game.
        /// </summary>
        void Resume();

        /// <summary>
        /// Stops the game.
        /// </summary>
        void StopGame();

        /// <summary>
        /// Gets information about the map that the game is played on.
        /// </summary>
        MapInfo GetMapInfo();

        /// <summary>
        /// Plays the async card.
        /// </summary>
        /// <param name="playedCard">The card being played.</param>
        /// <param name="usedChoice">The choice of the played card.</param>
        void PlayAsyncCard(CardInfo playedCard, CardInfo usedChoice = null);
    }
}