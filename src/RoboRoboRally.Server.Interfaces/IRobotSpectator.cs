﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to robot events.
    /// </summary>
    public interface IRobotSpectator : IClient
    {
        /// <summary>
        /// Announces that the energy level of a robot has changed.
        /// </summary>
        /// <param name="robot">The robot whose energy changed.</param>
        /// <param name="oldEnergy">The robot's old energy amount.</param>
        /// <param name="newEnergy">The robot's new energy amount.</param>
        void RobotEnergyChanged(RobotInfo robot, int oldEnergy, int newEnergy);

        /// <summary>
        /// Announces that the robot's lasers have activated.
        /// </summary>
        /// <param name="robot">The robot whose lasers have activated.</param>
        void RobotLasersActivated(RobotInfo robot);

        /// <summary>
        /// Announces that a robot has been deactivated and moved out of the map. Happens when the game is running and the player of that robot leaves the game.
        /// </summary>
        /// <param name="robot">The robot who has been deactivated.</param>
        void RobotDeactivated(RobotInfo robot);

        /// <summary>
        /// Announces that a robot has been activated and provides the public state of said robot.
        /// </summary>
        /// <param name="robotState"></param>
        void RobotActivated(RobotState robotState);
    }
}