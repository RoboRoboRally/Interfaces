﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to upgrade card events.
    /// </summary>
    public interface IUpgradeCardSpectator : IClient
    {
        /// <summary>
        /// Represents that upgrade cards were offered to a robot to buy.
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="offer"></param>
        void CardsOfferedInShop(RobotInfo robot, PurchaseOffer offer);

        /// <summary>
        /// Represents a card was bought.
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="boughtCard"></param>
        void CardBought(RobotInfo robot, PurchaseResponse boughtCard);

        /// <summary>
        /// Represents that no card was bought by the robot.
        /// </summary>
        /// <param name="robot"></param>
        void NoCardBought(RobotInfo robot);

        /// <summary>
        /// Represents that a robot was given a choice to use an upgrade card.
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="cardInfo"></param>
        void UpgradeCardOffered(RobotInfo robot, CardInfo cardInfo);

        /// <summary>
        /// Represents that an upgrade card was used.
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="cardSelection"></param>
        void UpgradeCardUsed(RobotInfo robot, CardInfo cardSelection);

        /// <summary>
        /// Represents that a robot did not use an upgrade card.
        /// </summary>
        /// <param name="robot"></param>
        void UpgradeCardNotUsed(RobotInfo robot);
    }
}