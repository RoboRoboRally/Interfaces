﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a revealed programming card.
    /// </summary>
    public class RevealedProgrammingCard
    {
        /// <summary>
        /// The robot who the card belongs to.
        /// </summary>
        public RobotInfo Robot { get; internal set; }

        /// <summary>
        /// The register the card was revealed in.
        /// </summary>
        public int Register { get; internal set; }

        /// <summary>
        /// The revealed card.
        /// </summary>
        public string RevealedCard { get; internal set; }
    }
}