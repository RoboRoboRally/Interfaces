﻿using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a server plugin. Needs to be implemented by every plugin of the server.
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// The info (name, version, etc.) about the plugin.
        /// </summary>
        PluginInfo PluginInfo { get; }

        /// <summary>
        /// Method that will be called first when initializing the plugin on the server.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Asks the plugin whether it can join the given lobby.
        /// </summary>
        /// <param name="lobby">The lobby in question.</param>
        /// <returns>True if the plugin can join the given lobby; False otherwise.</returns>
        bool CanJoinLobby(ILobbyInfo lobby);

        /// <summary>
        /// Adds the plugin to the lobby.
        /// </summary>
        /// <param name="lobby">The lobby the plugin is joining.</param>
        /// <returns>The implementation of the supported interfaces.</returns>
        IDictionary<Type, IClient> JoinLobby(ILobby lobby);

        /// <summary>
        /// Cleanup after itself when the plugin has been kicked from the lobby.
        /// </summary>
        /// <param name="lobby">The lobby being left.</param>
        void LeaveLobby(ILobby lobby);
    }

    /// <summary>
    /// Represents information about the plugin.
    /// </summary>
    public class PluginInfo
    {
        /// <summary>
        /// The name of the plugin.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The version of the plugin.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// The description of the plugin.
        /// </summary>
        public string Description { get; set; } = "Description not set";
    }

    /// <summary>
    /// Represents an instance of a plugin.
    /// </summary>
    public class InstantiatedPluginInfo
    {
        /// <summary>
        /// The instantied plugin.
        /// </summary>
        public PluginInfo Plugin { get; set; }

        /// <summary>
        /// Id of the instance of the plugin.
        /// </summary>
        public string InstanceId { get; set; }
    }
}