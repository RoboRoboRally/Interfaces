﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents an identifier that can be used to identify a connection.
    /// </summary>
    public class ConnectionInfo
    {
        /// <summary>
        /// The internal id of the connection.
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// The proclaimed name of the connection.
        /// </summary>
        public string ConnectionName { get; set; }

        /// <summary>
        /// The robot the connection may have selected. If no robot is selected (the connection represents a spectator), it is null.
        /// </summary>
        public RobotInfo SelectedRobot { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            var other = obj as ConnectionInfo;
            if (other is null)
                return false;

            return ConnectionId == other.ConnectionId;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return ConnectionId.GetHashCode();
        }

        /// <summary>
        /// Compares the two connection infos for equality of ConnectionId.
        /// </summary>
        public static bool operator ==(ConnectionInfo left, ConnectionInfo right)
        {
            if (ReferenceEquals(left, right))
                return true;

            return left?.Equals(right) ?? false;
        }

        /// <summary>
        /// Compares the two connection infos for inequality of ConnectionId.
        /// </summary>
        public static bool operator !=(ConnectionInfo left, ConnectionInfo right)
        {
            return !(left == right);
        }
    }
}