﻿using RoboRoboRally.Model.CardDB.Cards;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a card info. Used for both programming cards and upgrade cards.
    /// </summary>
    public class CardInfo
    {
        /// <summary>
        /// The name of the card.
        /// </summary>
        public string CardId { get; set; }
    }

    /// <summary>
    /// Represents an upgrade card info.
    /// </summary>
    public class UpgradeCardInfo : CardInfo
    {
        /// <summary>
        /// The type of the upgrade card
        /// </summary>
        public UpgradeCardType CardType { get; set; }

        /// <summary>
        /// The energy cost of buying the upgrade card
        /// </summary>
        public int Cost { get; set; }

        /// <summary>
        /// The choices of the card.
        /// </summary>
        public CardInfo[] Choices { get; set; }
    }
}