﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents public Read-Only information about the lobby.
    /// </summary>
    public interface ILobbyInfo
    {
        /// <summary>
        /// The Id of the lobby.
        /// </summary>
        string GetLobbyId();

        /// <summary>
        /// Gets a list of all currently available (i.e. not already taken) robots.
        /// </summary>
        /// <returns>The list of all currently available robots.</returns>
        RobotInfo[] GetAvailableRobots();

        /// <summary>
        /// Gets a list of all robots that can be picked in the game.
        /// The order the robots are returned at is fixed.
        /// </summary>
        RobotInfo[] GetAllRobots();
    }
}