﻿using RoboRoboRally.Model.CardDB.Cards;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents an purchase offer.
    /// </summary>
    public class PurchaseOffer
    {
        /// <summary>
        /// Represents how much energy the robot has to spend.
        /// </summary>
        public int Energy { get; set; }

        /// <summary>
        /// Owned cards, grouped by card type
        /// </summary>
        public Dictionary<UpgradeCardType, UpgradeCardInfo[]> OwnedCards { get; set; }

        /// <summary>
        /// The cards that are offered
        /// </summary>
        public UpgradeCardInfo[] OfferedCards { get; set; }

        /// <summary>
        /// maximum amount of cards for each card type
        /// </summary>
        public Dictionary<UpgradeCardType, int> OwnedCardLimit { get; set; }
    }
}