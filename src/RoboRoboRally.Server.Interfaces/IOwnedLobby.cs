﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a lobby that one created. Contains additional methods for removing additional connections
    /// </summary>
    public interface IOwnedLobby : ILobby
    {
        /// <summary>
        /// Adds a plugin into the current lobby.
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns>A list of all plugins that can still be added to the current lobby.</returns>
        PluginInfo[] AddPlugin(PluginInfo plugin);

        /// <summary>
        /// Returns data about instantiated plugins in the lobby.
        /// </summary>
        /// <returns></returns>
        InstantiatedPluginInfo[] GetAddedPlugins();

        /// <summary>
        /// Removes a connection from the lobby.
        /// </summary>
        /// <param name="connection">The connection to be removed.</param>
        void RemoveConnection(ConnectionInfo connection);

        /// <summary>
        /// Unselects a robot so that it can be picked by other clients.
        /// </summary>
        /// <param name="robot">The robot to free up.</param>
        void FreeRobot(RobotInfo robot);

        /// <summary>
        /// Removes a given instance of a plugin.
        /// </summary>
        /// <param name="plugin">The plugin to remove.</param>
        void RemovePlugin(InstantiatedPluginInfo plugin);
    }
}