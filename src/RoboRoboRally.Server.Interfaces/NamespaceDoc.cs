﻿using System.Runtime.CompilerServices;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// This namespace contains various interfaces and data classes used in inter-component communication of RoboRoboRally server and other components (mobile clients, game terminal, server plugins, etc.)
    /// </summary>
    [CompilerGenerated]
    public class NamespaceDoc
    {
    }
}