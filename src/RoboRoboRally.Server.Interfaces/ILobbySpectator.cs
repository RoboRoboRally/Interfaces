﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents events that occur inside the lobby.
    /// </summary>
    public interface ILobbySpectator : IClient
    {
        /// <summary>
        /// Represents that a robot has been picked by a client.
        /// </summary>
        void RobotPicked(string clientName, RobotInfo robot);

        /// <summary>
        /// Represents that a robot has been freed.
        /// </summary>
        void RobotFreed(string clientName, RobotInfo robot);

        /// <summary>
        /// Represents that a client joined the lobby.
        /// </summary>
        void ClientJoined(string clientName);

        /// <summary>
        /// Represents that a client left the lobby.
        /// </summary>
        void ClientLeft(string clientName);

        /// <summary>
        /// Represents that a lobby is closing.
        /// </summary>
        void LobbyClosing();

        /// <summary>
        /// Represents that a lobby closed. This is the last event called from a single lobby.
        /// </summary>
        void LobbyClosed();
    }
}