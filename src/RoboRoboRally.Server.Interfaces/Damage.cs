﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents the damage a player took.
    /// </summary>
    public class Damage
    {
        /// <summary>
        /// The cards that were drawn as a result of the damage.
        /// </summary>
        public string[] DamageCards { get; internal set; }
    }
}