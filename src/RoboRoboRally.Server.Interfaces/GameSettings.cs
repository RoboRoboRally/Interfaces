﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents settings of a new game.
    /// </summary>
    public class GameSettings
    {
        /// <summary>
        /// The map settings.
        /// </summary>
        public MapSettings MapSettings { get; set; }

        /// <summary>
        /// The seed used in the game. If null, a new seed will be generated
        /// </summary>
        public long? Seed { get; set; }

        /// <summary>
        /// Marks whether to include upgrades (upgrade cards, upgrade phase, etc.) or not.
        /// </summary>
        public bool IncludeUpgrades { get; set; } = false;

        /// <summary>
        /// Sets how many cards of each type will be present in the programming pack.
        /// </summary>
        public Dictionary<CardInfo, int> ProgrammingPack { get; set; }
    }

    /// <summary>
    /// Represents settings of a map.
    /// </summary>
    public class MapSettings
    {
        /// <summary>
        /// The main game plan ID.
        /// </summary>
        public string GamePlanId { get; set; }

        /// <summary>
        /// The starting plan ID.
        /// </summary>
        public string StartingPlanId { get; set; }

        /// <summary>
        /// The rotation of the main game plan.
        /// </summary>
        public RotationType GamePlanRotation { get; set; }

        /// <summary>
        /// The coordinates of the checkpoint tiles in the order they shall be passed through.
        /// </summary>
        public MapCoordinates[] CheckpointTiles { get; set; }

        /// <summary>
        /// The coordinates and the orientation of the priority antenna.
        /// </summary>
        public OrientedMapCoordinates PriorityAntenna { get; set; }

        /// <summary>
        /// The coordinates and the orientation of reboot tiles.
        /// </summary>
        public OrientedMapCoordinates[] RebootTiles { get; set; }
    }

    /// <summary>
    /// Represents a Coordinates-Direction pair.
    /// </summary>
    public class OrientedMapCoordinates
    {
        /// <summary>
        /// The coordinates. Obviously.
        /// </summary>
        public MapCoordinates Coordinates;

        /// <summary>
        /// The direction. Obviously.
        /// </summary>
        public Direction Direction;
    }

    /// <summary>
    /// Contains extension methods for <see cref="OrientedMapCoordinates"/>.
    /// </summary>
    public static class OrientedMapCoordinatesExtensions
    {
        /// <summary>
        /// Deconstructs <see cref="OrientedMapCoordinates"/> into coordinates and direction.
        /// </summary>
        public static void Deconstruct(this OrientedMapCoordinates orientedCoords, out MapCoordinates coordinates, out Direction direction)
        {
            coordinates = orientedCoords.Coordinates;
            direction = orientedCoords.Direction;
        }
    }
}