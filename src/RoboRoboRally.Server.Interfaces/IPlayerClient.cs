﻿using System;
using System.Threading.Tasks;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents an acting player. Must be implemented by player clients.
    /// </summary>
    public interface IPlayerClient : IClient
    {
        /// <summary>
        /// Requests the player to choose programming cards.
        /// </summary>
        /// <param name="count">The number of cards the player has to choose.</param>
        /// <param name="cards">The cards the player has to choose from.</param>
        /// <param name="timeout">Time during which the offer applies.</param>
        /// <returns>The <paramref name="count"/> amount of cards in the order the player wants them to be played.</returns>
        Task<CardInfo[]> ChooseProgrammingCards(int count, CardInfo[] cards, TimeSpan timeout);

        /// <summary>
        /// Requests the player to buy an upgrade card.
        /// </summary>
        /// <param name="offer">The offer being made</param>
        /// <param name="timeout">Time during which the offer applies.</param>
        /// <returns>The result.</returns>
        Task<PurchaseResponse> BuyUpgradeCard(PurchaseOffer offer, TimeSpan timeout);

        /// <summary>
        /// Requests the player to choose whether to play a choiceless ugprade card.
        /// </summary>
        /// <param name="card">The card being offered.</param>
        /// <param name="timeout">Time during which the offer applies.</param>
        /// <returns>Whether the player chooses to play the card.</returns>
        Task<bool> OfferChoicelessUpgradeCard(CardInfo card, TimeSpan timeout);

        /// <summary>
        /// Requests the player to choose whether to play an upgrade card with choices and if so, which choice he wishes to choose.
        /// </summary>
        /// <param name="card">The card being offered.</param>
        /// <param name="timeout">Time during which the offer applies.</param>
        /// <returns></returns>
        Task<CardInfo> OfferUpgradeCardWithChoices(UpgradeCardInfo card, TimeSpan timeout);

        /// <summary>
        /// Announces the player that one of his asynchronous cards is available to be played.
        /// </summary>
        /// <param name="card">The card that is available to be played now.</param>
        Task CardAvailableForPlay(UpgradeCardInfo card);

        /// <summary>
        /// Announces the player that one of his asynchronous cards is no longer available to be played.
        /// </summary>
        /// <param name="card">The card that can no longer be played now.</param>
        Task CardNoLongerAvailableForPlay(CardInfo card);

        /// <summary>
        /// Announces the player that his battery count has changed.
        /// </summary>
        /// <param name="newBatteryCount">The new amount of batteries.</param>
        Task BatteryCountChanged(int newBatteryCount);

        /// <summary>
        /// Announces the player that he can no longer pick his cards. Random cards will be selected for him.
        /// </summary>
        Task ProgrammingCardTimeout();
    }
}