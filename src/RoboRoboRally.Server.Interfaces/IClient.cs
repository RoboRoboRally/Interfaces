﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// The base interface representing a client.
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// Announces that the game has started.
        /// </summary>
        void GameStarted(IGame game);
    }
}