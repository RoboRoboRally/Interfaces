﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to game flow events.
    /// </summary>
    public interface IGameFlowSpectator : IClient
    {
        /// <summary>
        /// Announces that a turn has started.
        /// </summary>
        /// <param name="turnNumber">The turn number.</param>
        void TurnStarted(int turnNumber);

        /// <summary>
        /// Announces that a turn has ended.
        /// </summary>
        /// <param name="turnNumber">The turn number.</param>
        void TurnEnded(int turnNumber);

        /// <summary>
        /// Announces that a phase has started.
        /// </summary>
        /// <param name="phaseName">The name of the phase.</param>
        void PhaseStarted(string phaseName);

        /// <summary>
        /// Announces that a phase has ended.
        /// </summary>
        /// <param name="phaseName">The name of the phase.</param>
        void PhaseEnded(string phaseName);

        /// <summary>
        /// Announces that a game was paused.
        /// </summary>
        void GamePaused();

        /// <summary>
        /// Announces the a game was resumed.
        /// </summary>
        void GameResumed();

        /// <summary>
        /// Announces that a game has ended.
        /// </summary>
        void GameOver();

        /// <summary>
        /// Announces that a game is ending.
        /// </summary>
        void GameIsEnding();

        /// <summary>
        /// Announces that a robot has finished the race.
        /// </summary>
        /// <param name="robot">The robot who finished the race.</param>
        void RobotFinishedGame(RobotInfo robot);

        /// <summary>
        /// Announces that a robot has visited a checkpoint.
        /// </summary>
        /// <param name="robot">The robot that visited the checkpoint.</param>
        /// <param name="checkpointNr">The visited checkpoint's number.</param>
        void RobotVisitedCheckpoint(RobotInfo robot, int checkpointNr);

        /// <summary>
        /// Announces the priority in which the robots' actions will be performed.
        /// </summary>
        /// <param name="robotsInOrder">The ordering of the robots.</param>
        void RobotPriorityDetermined(RobotInfo[] robotsInOrder);
    }
}