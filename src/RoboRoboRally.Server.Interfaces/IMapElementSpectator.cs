﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to map elements being activated.
    /// </summary>
    public interface IMapElementSpectator : IClient
    {
        /// <summary>
        /// Announces that a map element activation has started.
        /// </summary>
        void MapElementActivationStarted();

        /// <summary>
        /// Announces that blue belts were activated.
        /// </summary>
        void BlueBeltsActivated();

        /// <summary>
        /// Announces that checkpoints were activated.
        /// </summary>
        void CheckpointsActivated();

        /// <summary>
        /// Announces that energy spaces were activated.
        /// </summary>
        void EnergySpacesActivated();

        /// <summary>
        /// Announces that green belts were activated.
        /// </summary>
        void GreenBeltsActivated();

        /// <summary>
        /// Announces that push panels were activated.
        /// </summary>
        void PushPanelsActivated();

        /// <summary>
        /// Announces that rotating gears were activated
        /// </summary>
        void RotatingGearsActivated();

        /// <summary>
        /// Announces that wall lasers were activated.
        /// </summary>
        void WallLasersActivated();
    }
}