﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to robots being moved around the map.
    /// </summary>
    public interface IMovementSpectator : IClient
    {
        /// <summary>
        /// Announces that a robot moved to another tile.
        /// </summary>
        /// <param name="robot">The robot that moved.</param>
        /// <param name="newCoords">The coordinates the <paramref name="robot"/> moved to.</param>
        void RobotMoved(RobotInfo robot, MapCoordinates newCoords);

        /// <summary>
        /// Announces that a robot got pushed.
        /// </summary>
        /// <param name="robot">The robot that was pushed.</param>
        /// <param name="oldCoords">The old coordinates of the pushed robot.</param>
        /// <param name="newCoords">The new coordinates of the pushed robot.</param>
        void RobotPushed(RobotInfo robot, MapCoordinates oldCoords, MapCoordinates newCoords);

        /// <summary>
        /// Announces that a robot teleported to another tile.
        /// </summary>
        /// <param name="robot">The robot that teleported.</param>
        /// <param name="newCoords">The coordinates the <paramref name="robot"/> teleported to.</param>
        /// <param name="direction">The direction the <paramref name="robot"/> is facing after the teleportation.</param>
        void RobotTeleported(RobotInfo robot, MapCoordinates newCoords, Direction direction);

        /// <summary>
        /// Announces that a robot is rebooting.
        /// </summary>
        /// <param name="robot">The robot that is rebooting.</param>
        /// <param name="mapCoordinates">The coordinates of the reboot token.</param>
        /// <param name="direction">The direction the robot will face once it reboots.</param>
        void RobotRebooting(RobotInfo robot, MapCoordinates mapCoordinates, Direction direction);

        /// <summary>
        /// Announces that a robot rotated around.
        /// </summary>
        /// <param name="robot">The robot that rotated</param>
        /// <param name="rotation">The type of rotation performed by the robot.</param>
        void RobotRotated(RobotInfo robot, RotationType rotation);

        /// <summary>
        /// Announces that a new robot joined the game.
        /// </summary>
        /// <param name="robot">The robot that joined the game.</param>
        /// <param name="coordinates">The intial coordinates the <paramref name="robot"/> will spawn at.</param>
        /// <param name="direction">The initial direction the <paramref name="robot"/> will face.</param>
        void RobotAdded(RobotInfo robot, MapCoordinates coordinates, Direction direction);

        /// <summary>
        /// Announces that a robot left the game.
        /// </summary>
        /// <param name="robot">The robot that left the game.</param>
        void RobotRemoved(RobotInfo robot);
    }
}