﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a robot identifier.
    /// </summary>
    public class RobotInfo
    {
        /// <summary>
        /// Name of the robot.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Color of the cards the robot is using.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// The zero-based order of the robot (for plotter so that it can identify them easily).
        /// </summary>
        public int Order { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is string)
                return Name.Equals(obj);

            var other = obj as RobotInfo;
            if (other is null)
                return false;

            return Name == other.Name;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Compares two RobotInfos
        /// </summary>
        public static bool operator ==(RobotInfo left, RobotInfo right)
        {
            return left?.Equals(right) ?? false;
        }

        /// <summary>
        /// Compares two RobotInfos
        /// </summary>
        public static bool operator !=(RobotInfo left, RobotInfo right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares a RobotInfo with a robot name.
        /// </summary>
        public static bool operator ==(RobotInfo info, string name)
        {
            return info?.Equals(name) ?? false;
        }

        /// <summary>
        /// Compares a RobotInfo with a robot name.
        /// </summary>
        public static bool operator !=(RobotInfo info, string name)
        {
            return !(info == name);
        }
    }
}