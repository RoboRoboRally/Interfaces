﻿using RoboRoboRally.Model.CardDB.Cards;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a public state of a robot.
    /// </summary>
    public class RobotState
    {
        /// <summary>
        /// The robot the state belongs to.
        /// </summary>
        public RobotInfo Robot { get; set; }

        /// <summary>
        /// The energy the robot has.
        /// </summary>
        public int Energy { get; set; }

        /// <summary>
        /// The last checkpoint the robot has passed. If no checkpoint was passed, 0 is provided (checkpoints are indexed from 1).
        /// </summary>
        public int LastPassedCheckpoint { get; set; }

        /// <summary>
        /// A collection of upgrade cards the robot has, grouped by the cards' type.
        /// </summary>
        public Dictionary<UpgradeCardType, UpgradeCardInfo[]> UpgradeCards { get; set; }

        /// <summary>
        /// The position and orientation of the robot.
        /// </summary>
        public OrientedMapCoordinates RobotPosition { get; set; }
    }
}