﻿namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// Represents a spectator who reacts to programming card events.
    /// </summary>
    public interface IProgrammingCardSpectator : IClient
    {
        /// <summary>
        /// Announces that new sets of cards were dealt to the players.
        /// </summary>
        void CardsDealt();

        /// <summary>
        /// Announces that cards were selected by the specified robot.
        /// </summary>
        /// <param name="robot">The robot who has finished his selection.</param>
        /// <param name="count">The amount of cards the robot picked.</param>
        void CardsSelected(RobotInfo robot, int count);

        /// <summary>
        /// Announces that all robots have selected their cards.
        /// </summary>
        void CardSelectionOver();

        /// <summary>
        /// Announces that a register with the specified number was activated.
        /// </summary>
        /// <param name="register">the register that was activated.</param>
        void RegisterActivated(int register);

        /// <summary>
        /// Announces that a card has been revealed.
        /// </summary>
        /// <param name="revealedCard">The revealed card.</param>
        void CardRevealed(RevealedProgrammingCard revealedCard);

        /// <summary>
        /// Announces that a card is being played.
        /// </summary>
        /// <param name="robot">The robot the card belongs to.</param>
        /// <param name="card">The card that is played.</param>
        void CardBeingPlayed(RobotInfo robot, string card);

        /// <summary>
        /// Announces that damage cards were drawn by the robot.
        /// </summary>
        /// <param name="robot">The robot who drew the damage cards.</param>
        /// <param name="damage">The damage cards</param>
        void DamageCardsTaken(RobotInfo robot, Damage damage);

        /// <summary>
        /// Announces that an already revealed card was replaced in its register.
        /// </summary>
        /// <param name="robot">The robot whose card was replaced.</param>
        /// <param name="register">the register that is being replaced.</param>
        /// <param name="oldCard">The old card.</param>
        /// <param name="newCard">The new card.</param>
        void RevealedCardReplaced(RobotInfo robot, int register, CardInfo oldCard, CardInfo newCard);

        /// <summary>
        /// Announces that a robot burned a card.
        /// </summary>
        /// <param name="robot">The robot who burned a card.</param>
        /// <param name="card">The burned card.</param>
        void CardBurned(RobotInfo robot, string card);

        /// <summary>
        /// Announces that a robot has not selected his cards on time.
        /// </summary>
        /// <param name="robot">The robot that has not selected the cards.</param>
        void CardsNotSelectedOnTime(RobotInfo robot);
    }
}