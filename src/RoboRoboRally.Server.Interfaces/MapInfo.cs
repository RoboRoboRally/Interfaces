﻿using RoboRoboRally.Model.MapDB;

namespace RoboRoboRally.Server.Interfaces
{
    /// <summary>
    /// The information about the map.
    /// </summary>
    public class MapInfo
    {
        /// <summary>
        /// The size of the map.
        /// </summary>
        public MapSize MapSize { get; set; }

        /// <summary>
        /// The settings the map was created from.
        /// </summary>
        public MapSettings Settings { get; set; }
    }
}